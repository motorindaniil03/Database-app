﻿using lab7.Models;
using lab7.Repositoies;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace lab7
{
    public partial class UpdateCars : Form
    {
        private int id;
        readonly CarsRepository Cr = new CarsRepository();
        public UpdateCars()
        {
            InitializeComponent();
        }
        public UpdateCars(Car car)
        {
            InitializeComponent();
            id = car.Id;
            textBox1.Text = car.CarNumber;
            textBox2.Text = car.CarBrand;
            textBox3.Text = car.Run.ToString();
            textBox4.Text = car.Status;
            textBox5.Text = car.Seats.ToString();
            textBox6.Text = car.Price.ToString();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string a1 = textBox1.Text;
            string a2 = textBox2.Text;
            int a3 = Convert.ToInt32 (textBox3.Text);
            string a4 = textBox4.Text;
            int a5 = Convert.ToInt32 (textBox5.Text);
            double a6 = Convert.ToDouble (textBox6.Text);
            Cr.Update(id, a1, a2, a3, a4, a5, a6);
            DialogResult= DialogResult.OK;
            Close();

        }
    }
}
