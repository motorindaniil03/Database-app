﻿using lab7.Repositoies;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using lab7.Models;

namespace lab7
{
    public partial class AddCar : Form
    {
        CarsRepository Cr= new CarsRepository();
        public AddCar()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string a1 = textBox1.Text;
            string a2 = textBox2.Text;
            int a3 = Convert.ToInt32(textBox3.Text);
            string a4 = textBox4.Text;
            int a5 = Convert.ToInt32(textBox5.Text);
            double a6 = Convert.ToDouble(textBox6.Text);

            Car car = new Car();
            car.CarNumber= a1;
            car.CarBrand= a2;
            car.Run = a3;
            car.Status= a4;
            car.Seats = a5;
            car.Price= a6;
            Cr.Create(car);
            DialogResult = DialogResult.OK;
            Close();

        }
    }
}
