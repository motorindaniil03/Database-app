﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using lab7.Models;
using lab7.Repositoies;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TaskbarClock;

namespace lab7
{
    
    public partial class UpdateOrder : Form
    {
        int id;
        OrdersRepository or = new OrdersRepository();
        public UpdateOrder(Order order)
        {
            InitializeComponent();
            id = order.Id;
            textBox1.Text = order.OrderNum.ToString();
            textBox2.Text = order.Car_Id.ToString();
            dateTimePicker1.Value = DateTime.Parse(order.OrderDate);
            dateTimePicker2.Value = DateTime.Parse(order.Time);
            textBox5.Text = Name;
            textBox6.Text = order.ClientID.ToString();
            dateTimePicker3.Value = DateTime.Parse(order.ReturnTime);
            textBox8.Text = order.HoursNumber.ToString();
        }

        private void update_Click(object sender, EventArgs e)
        {
            int a1 = Convert.ToInt32(textBox1.Text);
            int a2 = Convert.ToInt32(textBox2.Text);
            string a3 = dateTimePicker1.Value.ToString("dd/MM/yyyy");
            string a4 = dateTimePicker2.Value.ToString("HH:mm");
            string a5 = textBox5.Text;
            int a6 = Convert.ToInt32(textBox6.Text);
            string a7 = dateTimePicker3.Value.ToString("dd/MM/yyyy");
            int a8 = Convert.ToInt32(textBox8.Text);

            or.Update(id, a1, a2, a3, a4, a5, a6, a7, a8);
            DialogResult = DialogResult.OK;
            Close();



        }
    }
}
