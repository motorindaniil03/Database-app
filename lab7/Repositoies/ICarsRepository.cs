﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lab7.Models;

namespace lab7.Repositoies
{
    public interface ICarsRepository
    {
        void Create(Car car);

        public List<Car> Sort();
        List<Car> GetCars();
        void Delete(int id);

        void Update(int idcar, string carNumber, string carBrand, int run, string status,int seats, double price);

        Car GetCarById(int id);
        
        Car SearchCarByNumber(string name);

        List<Car> FiltrCarsByRun(int cost1, int cost2);

    }
}
