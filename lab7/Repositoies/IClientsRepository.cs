﻿using lab7.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab7.Repositoies
{
    public interface IClientsRepository
    {
        void Create(Client client);
        void Update(int clientId, string name, string passpotr, string adress, string telephone, string driverLicense);

        void Delete(Client client);

        Client GetClientById(int id);
        List<Client> GetClients();

    }
}
