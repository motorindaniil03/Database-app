﻿using lab7.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab7.Repositoies
{
    public interface IOrdersRepsitory
    {
        void Create(Order order);

        void Delete(Order order);

        void Update(int orderId, int orderNum,int Car_id,string orderDate,string Time,string employeeName, int ClientId,string ReturnTime,
            int HoursNum);
        Order GetorderById(int id);
        List<Order> Getorder();

        
    }
}
