﻿using lab7.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab7.Repositoies
{
    public class ClientsRepository : IClientsRepository
    {
        public void Create(Client client)
        {
            using (var db = new СarRentContext())
            {
                db.Clients.Add(client);
                db.SaveChanges();
            };
        }

        public void Delete(Client client)
        {
            using (var db = new СarRentContext())
            {
                db.Clients.Remove(client);
                db.SaveChanges();
            }
        }

        public Client GetClientById(int id)
        {
            using (var db = new СarRentContext())
            {
                return db.Clients.Where(c => c.Id == id).FirstOrDefault();
            }
        }

        public List<Client> GetClients()
        {
            using (var db = new СarRentContext())
            {
                return db.Clients.ToList();
            }
        }

        public void Update(int clientId, string name,string passport, string adress,string telephone,string driverLicense) 
        {
            using(var db = new СarRentContext())
            {
                var client1 = db.Clients.Where(c => c.Id == clientId).FirstOrDefault();
                client1.Name = name;
                client1.Passport= passport;
                client1.Adress= adress;
                client1.Telephone= telephone;
                client1.DriverLicense= driverLicense;
                db.SaveChanges();
                db.Clients.Update(client1);
                db.SaveChanges();
            }
        }
    }
}
