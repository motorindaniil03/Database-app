﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lab7.Models;

namespace lab7.Repositoies
{
    public class OrdersRepository : IOrdersRepsitory
    {
        public void Create(Order order)
        {
            using (var db =  new СarRentContext())
            {
                db.Orders.Add(order);
                db.SaveChanges();
            }
        }

        public void Delete(Order order)
        {
            using(var db = new СarRentContext())
            {
                db.Orders.Remove(order);
                db.SaveChanges();
            }
        }

        public List<Order> Getorder()
        {
            using (var db = new СarRentContext())
            {
                return db.Orders.ToList();
            }
        }

        public Order GetorderById(int id)
        {
            using (var db = new СarRentContext())
            {
                return db.Orders.Where(o => o.Id == id).FirstOrDefault();
            }
        }

        public void Update(int orderId, int orderNum, int Car_id, string orderDate, string Time, 
            string employeeName, int ClientId, string ReturnTime,int HoursNum)
        {
            using (var db = new СarRentContext())
            {
                var order1 = db.Orders.Where(c => c.Id == orderId).FirstOrDefault();
                order1.OrderNum = orderNum;
                order1.Car_Id = Car_id;
                order1.OrderDate = orderDate;
                order1.Time = Time;
                order1.EmployeeName = employeeName;
                order1.ClientID = ClientId;
                order1.ReturnTime = ReturnTime;
                order1.HoursNumber = HoursNum;
                db.SaveChanges();
                db.Orders.Update(order1);
                db.SaveChanges();

            }
        }
    }
}
