﻿using Microsoft.VisualBasic.ApplicationServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lab7.Models;

namespace lab7.Repositoies
{
    public class CarsRepository : ICarsRepository
    {

        
        public void Create(Car car)
        {
            using (var db = new СarRentContext())
            {
                db.Cars.Add(car);
                db.SaveChanges();
            }
        }


        public List<Car> Sort()
        {
            using (var db = new СarRentContext())
            {
                var sp1 = from c in GetCars()
                          orderby c.Price
                          select c;
                return sp1.ToList();
            }
           
        }
        public List<Car> GetCars()
        {
            using (var db = new СarRentContext())
            {
                return db.Cars.ToList();
            }
        }


        public void Delete(int id)
        {
            using (var db = new СarRentContext())
            {
                db.Cars.Remove(db.Cars.Where(c => c.Id == id).FirstOrDefault());
                db.SaveChanges();
            }
        }


        public void Update(int idcar, string carNumber, string carBrand, int run, string status,int seats, double price)
        {
            using (var db = new СarRentContext())
            {
                var car1 = db.Cars.Where(m => m.Id == idcar).FirstOrDefault();
                car1.CarNumber = carNumber;
                car1.CarBrand = carBrand;
                car1.Run = run;
                car1.Status = status;
                car1.Seats = seats;
                car1.Price = price;
                db.SaveChanges();
                db.Cars.Update(car1);
                db.SaveChanges();
            }
        }


        public Car GetCarById(int id)
        {
            using (var db = new СarRentContext())
            {
                return db.Cars.Where(m => m.Id == id).FirstOrDefault();
            }
        }

        public Car GetcarbyNumber(string carNumber)
        {
            using (var db = new СarRentContext())
            {
                return db.Cars.Where(c => c.CarNumber == carNumber).FirstOrDefault();
            }
        }

        public Car SearchCarByNumber(string carNumber)
        {
            using (var db = new СarRentContext())
            {
                return db.Cars.Where(c => c.CarNumber == carNumber ).FirstOrDefault();
            };
        }

        public List<Car> FiltrCarsByRun(int run1, int run2)
        {
            using (var db = new СarRentContext())
            {
                return db.Cars.Where(c => c.Run >= run1 && c.Run <= run2).ToList();
            }
        }
    }
}

