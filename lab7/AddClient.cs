﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using lab7.Repositoies;
using lab7.Models;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace lab7
{
    public partial class AddClient : Form
    {
        ClientsRepository Cr = new ClientsRepository();
        public AddClient()
        {
            InitializeComponent();
        }

        private void add_Click(object sender, EventArgs e)
        {
            string a0 = textBox1.Text;
            string a1 = textBox2.Text;
            string a2 = textBox3.Text;
            string a3 = textBox4.Text;
            string a4 = textBox5.Text;

            Client client = new Client();
            client.Name= a0;
            client.Passport=a1;
            client.Adress=a2;
            client.Telephone=a3;
            client.DriverLicense=a4;

            Cr.Create(client);
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
