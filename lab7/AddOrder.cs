﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using lab7.Models;
using lab7.Repositoies;

namespace lab7
{
    public partial class AddOrder : Form
    {
        OrdersRepository or = new OrdersRepository();
        public AddOrder()
        {
            InitializeComponent();
        }

        private void add_Click(object sender, EventArgs e)
        {
            int a1 = Convert.ToInt32(textBox1.Text);
            int a2 = Convert.ToInt32(textBox2.Text);
            string a3 = dateTimePicker1.Value.ToString("dd/MM/yyyy");
            string a4 = dateTimePicker2.Value.ToString("HH:mm");
            string a5 = textBox5.Text;
            int a6 = Convert.ToInt32(textBox6.Text);
            string a7 = dateTimePicker3.Value.ToString("dd/MM/yyyy");
            int a8 = Convert.ToInt32(textBox8.Text);

            Order order = new Order();
            order.OrderNum = a1;
            order.Car_Id = a2;
            order.OrderDate = a3;
            order.Time = a4;
            order.EmployeeName= a5;
            order.ClientID= a6;
            order.ReturnTime = a7;
            order.HoursNumber = a8;

            or.Create(order);
            DialogResult= DialogResult.OK;
            Close();

        }
    }
}
