﻿namespace lab7
{
    partial class Menu
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.querys = new System.Windows.Forms.Button();
            this.customer = new System.Windows.Forms.Button();
            this.orders = new System.Windows.Forms.Button();
            this.cars = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // querys
            // 
            this.querys.Location = new System.Drawing.Point(363, 218);
            this.querys.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.querys.Name = "querys";
            this.querys.Size = new System.Drawing.Size(139, 69);
            this.querys.TabIndex = 8;
            this.querys.Text = "Запрос";
            this.querys.UseVisualStyleBackColor = true;
            this.querys.Click += new System.EventHandler(this.querys_Click);
            // 
            // customer
            // 
            this.customer.Location = new System.Drawing.Point(121, 218);
            this.customer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.customer.Name = "customer";
            this.customer.Size = new System.Drawing.Size(139, 69);
            this.customer.TabIndex = 7;
            this.customer.Text = "Заказчик";
            this.customer.UseVisualStyleBackColor = true;
            this.customer.Click += new System.EventHandler(this.customer_Click_1);
            // 
            // orders
            // 
            this.orders.Location = new System.Drawing.Point(363, 50);
            this.orders.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.orders.Name = "orders";
            this.orders.Size = new System.Drawing.Size(139, 69);
            this.orders.TabIndex = 6;
            this.orders.Text = "Заказы";
            this.orders.UseVisualStyleBackColor = true;
            this.orders.Click += new System.EventHandler(this.orders_Click_1);
            // 
            // cars
            // 
            this.cars.Location = new System.Drawing.Point(121, 50);
            this.cars.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cars.Name = "cars";
            this.cars.Size = new System.Drawing.Size(139, 69);
            this.cars.TabIndex = 5;
            this.cars.Text = "Машины";
            this.cars.UseVisualStyleBackColor = true;
            this.cars.Click += new System.EventHandler(this.cars_Click_1);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 356);
            this.Controls.Add(this.querys);
            this.Controls.Add(this.customer);
            this.Controls.Add(this.orders);
            this.Controls.Add(this.cars);
            this.Name = "Menu";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion
        private Button querys;
        private Button customer;
        private Button orders;
        private Button cars;
    }
}