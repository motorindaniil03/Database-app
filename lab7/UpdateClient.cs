﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using lab7.Models;
using lab7.Repositoies;

namespace lab7
{
    public partial class UpdateClient : Form
    {
        int id;
        ClientsRepository cr = new ClientsRepository();
        public UpdateClient(Client client)
        {
            InitializeComponent();
            id = client.Id;
            textBox1.Text = client.Name;
            textBox2.Text = client.Passport;
            textBox3.Text = client.Adress;
            textBox4.Text = client.Telephone;
            textBox5.Text = client.DriverLicense;
        }

        private void change_Click(object sender, EventArgs e)
        {
            string a0 = textBox1.Text;
            string a1 = textBox2.Text;
            string a2 = textBox3.Text;
            string a3 = textBox4.Text;
            string a4 = textBox5.Text;
            DialogResult = DialogResult.OK;
            cr.Update(id,a0, a1, a2, a3, a4);
            Close();
        }
    }
}
