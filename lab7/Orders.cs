﻿using lab7.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using lab7.Models;
using lab7.Repositoies;

namespace lab7
{
    public partial class Orders : Form
    {
        int Id;
        OrdersRepository or = new OrdersRepository();
        public Orders()
        {
            InitializeComponent();
            UpdateGrid();
        }
        private void UpdateGrid()
        {
            using (СarRentContext db = new СarRentContext())
            {
                dataGridView1.DataSource = db.Orders.ToList();
            }
        }
        private void SelectId()
        {
            Id = (int)(dataGridView1[0, dataGridView1.CurrentCell.RowIndex].Value);
            if (Id == 0)
            {
                MessageBox.Show("Выберите запись!");
            }
        }
        private void Orders_Load(object sender, EventArgs e)
        {
            UpdateGrid();
        }

        private void add_Click(object sender, EventArgs e)
        {
            var f = new AddOrder();
            if (f.ShowDialog() == DialogResult.OK) { UpdateGrid(); }
        }

        private void change_Click(object sender, EventArgs e)
        {
            SelectId();
            var f = new UpdateOrder(or.GetorderById(Id));
            if (f.ShowDialog() == DialogResult.OK) { UpdateGrid(); }
        }

        private void delete_Click(object sender, EventArgs e)
        {
            SelectId();
            or.Delete(or.GetorderById(Id));
            UpdateGrid();
        }
    }
}
