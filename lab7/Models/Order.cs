﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab7.Models
{
    public class Order
    {
        public int Id { get; set; }
        public int OrderNum { get; set; }
        public int Car_Id { get; set; }
        public string OrderDate { get; set; }
        public string Time { get; set; }
        public string EmployeeName { get; set; }
        public int ClientID { get; set; }
        public string ReturnTime { get; set; }
        public int HoursNumber { get; set; }
    }
}
