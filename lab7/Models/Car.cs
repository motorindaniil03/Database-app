﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab7.Models
{
    public class Car
    {
        public int Id { get; set; }
        public string CarNumber { get; set; }
        public string CarBrand { get; set; }
        public int Run { get; set; }
        public string Status { get; set; }
        public int Seats { get; set; }
        public double Price { get; set; }
    }
}
