﻿using lab7;
using lab7.Models;
using Microsoft.EntityFrameworkCore;
using System.Configuration;

namespace lab7.Models
{
    public class СarRentContext : DbContext
    {
        public DbSet<Car> Cars { get; set; }
        
        public DbSet<Order> Orders { get; set; }
        public DbSet<Client> Clients { get; set; } 

        public СarRentContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        }
    }
}