﻿namespace lab7
{
    partial class Cars
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.showAll = new System.Windows.Forms.Button();
            this.filtrRun = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Run1 = new System.Windows.Forms.NumericUpDown();
            this.sort = new System.Windows.Forms.Button();
            this.delete = new System.Windows.Forms.Button();
            this.change = new System.Windows.Forms.Button();
            this.add = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Run2 = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Run1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Run2)).BeginInit();
            this.SuspendLayout();
            // 
            // showAll
            // 
            this.showAll.Location = new System.Drawing.Point(280, 486);
            this.showAll.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.showAll.Name = "showAll";
            this.showAll.Size = new System.Drawing.Size(157, 73);
            this.showAll.TabIndex = 35;
            this.showAll.Text = "Показать все ";
            this.showAll.UseVisualStyleBackColor = true;
            this.showAll.Click += new System.EventHandler(this.showAll_Click);
            // 
            // filtrRun
            // 
            this.filtrRun.Location = new System.Drawing.Point(280, 386);
            this.filtrRun.Name = "filtrRun";
            this.filtrRun.Size = new System.Drawing.Size(157, 75);
            this.filtrRun.TabIndex = 33;
            this.filtrRun.Text = "Фильтрация по пробегу";
            this.filtrRun.UseVisualStyleBackColor = true;
            this.filtrRun.Click += new System.EventHandler(this.filtrRun_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(500, 358);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 28);
            this.label4.TabIndex = 31;
            this.label4.Text = "от";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(485, 315);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(308, 28);
            this.label1.TabIndex = 28;
            this.label1.Text = "Фильтрация машин по пробегу ";
            // 
            // Run1
            // 
            this.Run1.Location = new System.Drawing.Point(574, 363);
            this.Run1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Run1.Maximum = new decimal(new int[] {
            500000,
            0,
            0,
            0});
            this.Run1.Name = "Run1";
            this.Run1.Size = new System.Drawing.Size(137, 27);
            this.Run1.TabIndex = 24;
            // 
            // sort
            // 
            this.sort.Location = new System.Drawing.Point(280, 292);
            this.sort.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.sort.Name = "sort";
            this.sort.Size = new System.Drawing.Size(157, 73);
            this.sort.TabIndex = 23;
            this.sort.Text = "Сортировка по цене за час";
            this.sort.UseVisualStyleBackColor = true;
            this.sort.Click += new System.EventHandler(this.sort_Click);
            // 
            // delete
            // 
            this.delete.Location = new System.Drawing.Point(68, 486);
            this.delete.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(157, 73);
            this.delete.TabIndex = 22;
            this.delete.Text = "Удалить машину";
            this.delete.UseVisualStyleBackColor = true;
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // change
            // 
            this.change.Location = new System.Drawing.Point(68, 388);
            this.change.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.change.Name = "change";
            this.change.Size = new System.Drawing.Size(157, 73);
            this.change.TabIndex = 21;
            this.change.Text = "Изменить машину";
            this.change.UseVisualStyleBackColor = true;
            this.change.Click += new System.EventHandler(this.change_Click);
            // 
            // add
            // 
            this.add.Location = new System.Drawing.Point(68, 292);
            this.add.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(157, 73);
            this.add.TabIndex = 20;
            this.add.Text = "Добавить машину";
            this.add.UseVisualStyleBackColor = true;
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(50, 3);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 25;
            this.dataGridView1.Size = new System.Drawing.Size(1029, 264);
            this.dataGridView1.TabIndex = 19;
            // 
            // Run2
            // 
            this.Run2.Location = new System.Drawing.Point(574, 461);
            this.Run2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Run2.Maximum = new decimal(new int[] {
            1500000,
            0,
            0,
            0});
            this.Run2.Name = "Run2";
            this.Run2.Size = new System.Drawing.Size(137, 27);
            this.Run2.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(500, 460);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 28);
            this.label5.TabIndex = 32;
            this.label5.Text = "до";
            // 
            // Cars
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1252, 562);
            this.Controls.Add(this.showAll);
            this.Controls.Add(this.filtrRun);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Run2);
            this.Controls.Add(this.Run1);
            this.Controls.Add(this.sort);
            this.Controls.Add(this.delete);
            this.Controls.Add(this.change);
            this.Controls.Add(this.add);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Cars";
            this.Text = "Cars";
            this.Load += new System.EventHandler(this.Cars_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Run1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Run2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Button showAll;
        private Button filtrRun;
        private Label label4;
        private Label label1;
        private NumericUpDown Run1;
        private Button sort;
        private Button delete;
        private Button change;
        private Button add;
        private DataGridView dataGridView1;
        private NumericUpDown Run2;
        private Label label5;
    }
}