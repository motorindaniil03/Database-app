﻿using lab7.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using lab7.Repositoies;

namespace lab7
{
    public partial class Cars : Form
    {
        int id;
        readonly CarsRepository Cr = new CarsRepository();
        public int Id { get; set; }
        public Cars()
        {
            InitializeComponent();
        }
        private void UpdateGrid()
        {
            using (СarRentContext db = new СarRentContext())
            {
                dataGridView1.DataSource = db.Cars.ToList();
            }
        }
        private int SelectId()
        {
            id = (int)(dataGridView1.CurrentCell.Value);
            while (true)
            {
                if (id == 0)
                {
                    MessageBox.Show("Выберите запись!");
                }
                else
                {
                    return id;
                }
            }
        }

        private void Cars_Load(object sender, EventArgs e)
        {
            UpdateGrid();
        }

        private void add_Click(object sender, EventArgs e)
        {
            var f = new AddCar();
            if (f.ShowDialog() == DialogResult.OK) { UpdateGrid();}
            
        }

        private void change_Click(object sender, EventArgs e)
        {
            var f = new UpdateCars(Cr.GetCarById(SelectId()));
            f.Show();
            UpdateGrid();
        }

        private void delete_Click(object sender, EventArgs e)
        {
            Cr.Delete(SelectId());
        }

        private void showAll_Click(object sender, EventArgs e)
        {
            UpdateGrid();
        }

        private void sort_Click(object sender, EventArgs e)
        {  
            dataGridView1.DataSource = Cr.Sort();
        }

        private void filtrRun_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Cr.FiltrCarsByRun(Convert.ToInt32(Run1.Value),Convert.ToInt32(Run2.Value));
        }
    }
}
