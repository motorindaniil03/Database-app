﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using lab7.Models;
using lab7.Repositoies;
using Microsoft.EntityFrameworkCore;

namespace lab7
{
    public partial class Querys : Form
    {
        public Querys()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            //1
            if (comboBox1.SelectedIndex == 0)
            {
                dateTimePicker1.Visible = true;
                query_text.Text = "•\tВывести список заказчиков, взявших машину на прокат в заданную дату.";

            }
            if (comboBox1.SelectedIndex != 0)
            {
                dateTimePicker1.Visible = false;
            }


            //2
            if (comboBox1.SelectedIndex == 1)
            {
                query_text.Text = "•\tВывести список самых популярных машин (заказанных более 2 раз).";
            }

            //4
            if (comboBox1.SelectedIndex == 2)
            {
                query_text.Text = "•\tВывести таблицу с Заказами";
            }

        }

        private void run_Click(object sender, EventArgs e)
        {
            
            dataGridView1.Visible = true;

            //1
            if (comboBox1.SelectedIndex == 0)
            {
                using( var db = new СarRentContext())
                {
                    dataGridView1.DataSource = (from client in db.Clients join order in db.Orders on client.Id equals order.ClientID 
                                               where order.OrderDate == dateTimePicker1.Value.ToString("dd.MM.yyyy") 
                                               select new {Name = client.Name}).ToList();
                
                   
                }
            }


            //2
            if (comboBox1.SelectedIndex == 1)
            {
                using(var db = new СarRentContext())
                {
                    dataGridView1.DataSource = (from order in db.Orders join car in db.Cars on order.Car_Id equals car.Id 
                                                group order  by car.CarNumber  into g where g.Count() > 2 select new { CarNumber = g.Key} ).ToList();
                }
            }


            //3
            if (comboBox1.SelectedIndex == 2)
            {
                using (var db = new  СarRentContext())
                {
                    dataGridView1.DataSource = (from order in db.Orders select order).ToArray();
                }
            }


        }
    }
}
